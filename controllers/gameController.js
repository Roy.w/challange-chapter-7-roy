const {Games,Room} = require("../models")

const findWinner = (player_1,player_2) =>{
    if(player_1 === "rock" ){
        if(player_2 === "rock"){
            return "draw"
        }
        if(player_2 === "paper"){
            return "lose"
        }
        if(player_2 === "scissor"){
            return "win"
        }
    }
    if(player_1 === "paper"){
        if(player_2 === "paper"){
            return "draw"
        }
        if(player_2 === "scissor"){
            return "lose"
        }
        if(player_2 === "rock"){
            return "win"
        }
    }
    if(player_1 === "scissor"){
        if(player_2 === "scissor"){
            return "draw"
        }
        if(player_2 === "rock"){
            return "lose"
        }
        if(player_2 === "paper"){
            return "win"
        }
    }
}

exports.multiplayer = async (req,res)=>{
    const {UserId,RoomId,result} = req.body;

    const isNewGame = await Games.findAll({where:{room_id:RoomId}});
    const isUserExist = await Games.findOne({
        where:{ room_id:RoomId, user_id:UserId}
    });
    if(isNewGame.length === 0){
        Games.create({ user_id:UserId,room_id:RoomId,result})
        .then((respon)=>{
            res.status(201).json({status:"Create game succes",respon})
        })
        .catch((err)=>{
            res.status(400).json({status:"create Game Failed",message:err})
        })
    }else if(isNewGame.length === 1){
        if(isNewGame.length === 1){
            if(!isUserExist){
                const gameResult = findWinner(isNewGame[0].result,result);
                Games.create({ user_id:UserId,room_id:RoomId,result})
                .then(()=>{
                    Room.update({
                        result:gameResult,player: isNewGame[0].user_id
                    },{where:{id:RoomId}})
                    .then(()=>{
                        res.json({result: `Player One ${gameResult}`});
                    })
                    .catch((err)=>{
                        res.status(400).json({status:"create game failed",message:err})
                    })
                })
                .catch((err)=>{
                    res.status(400).json({status:"create game failed",message:err})
                });

            }else{
                res.status(400).json({
                    status: "create game failed",
                    message:"this player is inserted",
                })
            }
        }
    }
}




