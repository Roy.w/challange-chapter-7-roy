const express = require('express');
const router = express.Router();
const pageRouter = require("./page");
const authRouter = require("./auth");
const roomRouter = require("./room")
const gamesRouter = require("./games")



/* GET home page. */


router.use("/page", pageRouter);
router.use("/auth", authRouter);
router.use("/room", roomRouter);
router.use("/games", gamesRouter);



module.exports = router;
