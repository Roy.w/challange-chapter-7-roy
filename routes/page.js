const router = require("express").Router();
const page = require("../controllers/pageController");


router.get("/login",page.login);
router.get("/Register",page.register);
router.get("/room",page.room);


module.exports = router