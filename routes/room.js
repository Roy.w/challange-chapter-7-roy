const router = require("express").Router();
const room = require("../controllers/roomController");
const restrict = require("../middleware/restrict")




router.get("/getRoom",room.get);
router.post("/", room.createRoom);


module.exports = router