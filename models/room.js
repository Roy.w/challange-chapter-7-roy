'use strict';
const jwt = require("jsonwebtoken")

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Room.belongsTo(models.User,{
        foreignKey:"player",
        as:"user"
      })

    }
    generateToken = () => {
      // Jangan memasukkan password ke dalam payload
      const payload = {
      id: this.id,
      name: this.name,
      result: this.result,
      player: this.player
      }
      // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const rahasia = 'Ini rahasia ga boleh disebar-sebar'
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, rahasia)
      return token
      }

  }
  Room.init({
    name: DataTypes.STRING,
    result: DataTypes.STRING,
    player: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};